import React from "react";
// import "./App.css";
import Picture from "./components/pictureCard";
import Profile from "./components/profile";

function App() {
  return (
    <div className="App">
      <Profile />
      <Picture />
      <Picture />
      <Picture />
    </div>
  );
}

export default App;
