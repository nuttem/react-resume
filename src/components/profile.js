import React, { Component } from "react";
import "./profile.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTh,
  faTv,
  faTag,
  faBookmark,
  faCog
} from "@fortawesome/free-solid-svg-icons";

class Profile extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="profile-area">
          <div className="profile">
            <div className="profile-picture">
              <img
                src="https://instagram.fbkk2-5.fna.fbcdn.net/v/t51.2885-19/s150x150/74366019_788850271534692_2646449854410129408_n.jpg?_nc_ht=instagram.fbkk2-5.fna.fbcdn.net&_nc_ohc=7KVzGdwF81IAX98CG7Z&oh=841042c16d7f15edcc23d1f269f0cc61&oe=5EBAA3F6"
                alt=""
              />
              <div className="profile-info">
                <div className="profile-info-top">
                  <h1>NUTTEM</h1>
                  <a href="#" className="edit">
                    Edit Profile
                  </a>
                  <FontAwesomeIcon
                    icon={faCog}
                    // style={{ color: #333; }}
                  />
                </div>
                <div className="profile-info-center">
                  <span>
                    <strong>157</strong>posts
                  </span>
                  <span>
                    <strong>708</strong>followers
                  </span>
                  <span>
                    <strong>563</strong>following
                  </span>
                </div>
                <div className="profile-info-bottom">
                  <strong>NUTTEM</strong>
                  <br />
                  <a href="github" target="_blank">
                    github.com
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div>
          <div className="tabs">
            <div onClick={this.handleChange} className="tab-item active">
              <FontAwesomeIcon icon={faTh} />
              <strong>POSTS</strong>
            </div>
            <div onClick={this.handleChange} className="tab-item">
              <FontAwesomeIcon icon={faTv} />
              <strong>IGTV</strong>
            </div>
            <div onClick={this.handleChange} className="tab-item">
              <FontAwesomeIcon icon={faBookmark} />
              <strong>SAVED</strong>
            </div>
            <div onClick={this.handleChange} className="tab-item">
              <FontAwesomeIcon icon={faTag} />
              <strong>TAGGED</strong>
            </div>
          </div>
        </div>

        <div id="tb-2" className="tabs-content-item tb-2">
          <i>
            <FontAwesomeIcon icon={faTv} />
          </i>
          <h1>Upload a Video</h1>
          <p>Videos must be between 1 and 60 minutes long.</p>
          <a href="#" className="tb-2-btn">
            Upload
          </a>
        </div>

        <div id="tb-3" className="tab-content-item tb-3">
          {/* <p>Icon</p> */}
          Only you can see what you've saved
        </div>

        <div id="tb-4" className="tab-content-item tb-4">
          {/* <p>Icon</p> */}
          Tags
        </div>
      </div>
    );
  }
}

export default Profile;
