import React, { Component } from "react";
import "./pictureCard.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart, faComment } from "@fortawesome/free-solid-svg-icons";

class Card extends React.Component {
  render() {
    return (
      <div className="tabs-content">
        <div id="tb-1" className="tabs-content-item">
          <div className="gallery-grid">
            <div className="grid-img">
              <div className="grid-img-icon">
                <span>
                  <FontAwesomeIcon icon={faHeart} /> 100
                </span>
                <span>
                  <FontAwesomeIcon icon={faComment} /> 100
                </span>
              </div>
              <img
                src="https://images.unsplash.com/photo-1502085671122-2d218cd434e6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1398&q=80"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Card;
