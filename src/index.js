import React from "react";
import ReactDOM from "react-dom";
// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import { facog } from "@fortawesome/free-solid-svg-icons";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

// const element = <FontAwesomeIcon icon={facog} />;

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
